/**
 * Created by Anmol on 2/4/2016.
 */
public class EditDistance {

    public static void main(String[] args) {
        String str1 = "Saturday";
        String str2 = "Sunday";

        int i1 = str1.length();
        int i2 = str2.length();
        int[][] result = new int[i1 + 1][i2 + 1];

        for (int i = 0; i <= i1; i++) {
            for (int j = 0; j <= i2; j++) {
                if (i == 0) result[i][j] = j;
                else if (j == 0) result[i][j] = i;

                else {
                    if (str1.charAt(i - 1) == str2.charAt(j - 1)) {
                        result[i][j] = result[i - 1][j - 1];
                    } else {

                        result[i][j] =1+ Math.min(Math.min(result[i][j-1], result[i-1][j]), result[i-1][j-1]);
                    }
                }
            }
        }
        System.out.print(result[i1][i2]);
    }
}
