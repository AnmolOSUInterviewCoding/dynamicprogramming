/**
 * Created by Anmol on 1/12/2016.
 */
public class HouseRobber {

    public static void main(String[] args) {
        int[] arr = new int[]{12,3,4,55,10,23,24};

        int inclusive = 0;
        int exclusive =0;

        for(int i =0; i<arr.length;i++) {
            int temp = (inclusive > exclusive)? inclusive: exclusive;
            inclusive = exclusive + arr[i];
            exclusive = temp;
            System.out.println("inclusive: " + inclusive + " Exclusive: " + exclusive);

        }
    }

}
