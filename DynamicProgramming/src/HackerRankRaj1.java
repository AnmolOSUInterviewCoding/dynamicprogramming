
/**
 * @author Anmol on 2/8/2016.
 */

//Remove all occurences of an element in the linked list
public class HackerRankRaj1 {

    public static class LinkedListNode {
        int val;
        LinkedListNode next;

        public LinkedListNode(int val) {
            this.val = val;
            this.next = null;
        }
    }

    public static void main(String[] args) {
        LinkedListNode head = new LinkedListNode(1);
        head.next = new LinkedListNode(1);
        head.next.next = new LinkedListNode(1);
        head.next.next = new LinkedListNode(2);
        head.next.next.next = new LinkedListNode(3);
        head.next.next.next.next = new LinkedListNode(4);
        head.next.next.next.next.next = new LinkedListNode(5);
        head.next.next.next.next.next.next = new LinkedListNode(1);


        removeElement(head, 1);
    }

    private static void removeElement(LinkedListNode head, int value) {
        LinkedListNode temp = head;
        LinkedListNode current = null;
        //if head itself is the val to be deleted.
        while(temp.val == value && temp!=null) {
            head = temp.next;
            temp = head;
        }
        //delete all other occurences.
        while(temp!=null) {
            while(temp!=null && temp.val != value) {
                current = temp;
                temp = temp.next;
            }

            if(temp == null) return;

            current.next = temp.next;

            temp= current.next;
        }

    }
}
