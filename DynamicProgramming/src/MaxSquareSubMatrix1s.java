/**
 * Created by Anmol on 2/8/2016.
 */
public class MaxSquareSubMatrix1s {

    public static void main(String[] args) {

        int matrix[][] =  new int[][]{{0, 1, 1, 0, 1},
            {1, 1, 0, 1, 0},
            {0, 1, 1, 1, 0},
            {1, 1, 1, 1, 0},
            {1, 1, 1, 1, 1},
            {0, 0, 0, 0, 0}};

        int[][] result = new int[matrix.length][matrix[0].length];
        for(int i = 0; i<matrix.length;i++) {
            result[i][0] = matrix[i][0];
        }
        for(int i = 0; i<matrix[0].length;i++) {
            result[0][i] = matrix[0][i];
        }

        for(int i =1 ;i< matrix.length;i++) {
            for(int j = 1;j<matrix[0].length;j++) {
                if(matrix[i][j] == 1) {
                    result[i][j] = 1 + Math.min(Math.min(result[i-1][j], result[i][j-1]), result[i-1][j-1]);
                } else {
                    result[i][j] = 0;
                }

            }
        }

        for(int i = 0; i<result.length;i++) {
            for(int j = 0; j<result[0].length;j++) {
                System.out.print(result[i][j] +" ");
            }
            System.out.println();
        }
    }
}
