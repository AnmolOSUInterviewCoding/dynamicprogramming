/**
 * Created by Anmol on 2/9/2016.
 */
public class IterativePowerOfN {

    public static void main(String[] args) {
        int n = 2;
        int power = 5;
        int ans = 1;
        while (power > 0) {
            /*ans = ans*n;
            power--;*/
            if (power % 2 != 0) {
                ans = ans * n;
            }
            n = n * n;

            power = power / 2;
        }
        System.out.print(ans);

    }
}
