/**
 * Created by Anmol on 1/17/2016.
 */
public class NumberOfDigitsOne {

    public static void main(String[] args) {
        int n = 31;
        int[] result = new int[n + 1];
        result[0] = 0;
        for (int i = 1; i <= n; i++) {
            if (i % 10 == 1) {
                result[i] = 1 + result[i - 1];

            }
            if(i/10 == 1) {
                result[i] = 1 + result[i-1];
            }
            else {
                result[i] = result[i - 1];
            }
        }
        int j = 0;
        for (int i : result) {
            System.out.println("value of : " + j + "is: " + i);
            j++;
        }

    }
}
