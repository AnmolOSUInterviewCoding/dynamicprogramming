/**
 * Created by Anmol on 9/24/2016.
 */
public class FrogJumpLeetCode {
    public static void main(String[] args) {
        FrogJumpLeetCode frogJumpLeetCode = new FrogJumpLeetCode();
        int[] stones = new int[]{0,1,3,6,10,15,16,21};
        frogJumpLeetCode.canCross(stones);
    }

    private boolean canCross(int[] stones) {
        if(stones.length <=1) return true;
        boolean canJump = false;
        int[] currentK = new int[stones.length];
        boolean[] canReach = new boolean[stones.length];

        if(stones[1]>1) return false;
        currentK[0] = 1;
        currentK[1] = 1;
        canReach[0] = true;
        canReach[1] = true;
        for(int i =2;i<stones.length;i++) {

            for(int j = 0;j<i;j++) {
                if((stones[i]-stones[j]==currentK[j]+1 || stones[i]-stones[j]==currentK[j] || stones[i]-stones[j]==currentK[j]-1)&&canReach[j]) {
                    canReach[i] = true;
                    currentK[i] = Math.max(currentK[i], stones[i] - stones[j]);
                }
            }

        }
        return canReach[stones.length-1];
    }
}
