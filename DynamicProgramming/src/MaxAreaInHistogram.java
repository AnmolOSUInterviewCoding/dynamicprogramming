import java.util.Stack;

/**
 * Created by Anmol on 2/8/2016.
 */
public class MaxAreaInHistogram {

    public static void main(String[] args) {
        int arr[] = new int[]{2,1,2,3,1};
        Stack<Integer> st = new Stack<Integer>();
        int i = 0;
        int pop;
        int area = 0, maxArea = 0;
        while(i<arr.length) {
            if(st.empty()|| arr[st.peek()] < arr[i]) {
                st.push(i++);
            } else {
                area = getMaxArea(arr, st, i, maxArea);
            }
        }
        while (!st.empty()) {
            area = getMaxArea(arr, st, i, maxArea);
        }

        System.out.print(area);
    }

    private static int getMaxArea(int[] arr, Stack<Integer> st, int i, int maxArea) {
        int pop;
        int area;
        pop = st.pop();
        if(st.empty()) {
            area = arr[pop] * i;
        } else {
            area = arr[pop]* (i - st.peek() -1);
        }
        if(area>maxArea) {
            maxArea = area;
        }
        return maxArea;
    }
}
