/**
 * Created by Anmol on 1/13/2016.
 */
public class PerfectSquare {

    public static void main(String[] args) {
        int n = 13;
        int[] result = new int[n+1];
        //1,4,9,16,...
        result[0] = 0;
        result[1] = 1;
        for(int i = 2;i<=n;i++) {
            result[i] = Integer.MAX_VALUE;
            for(int x = 1; x * x <= i; x++){
                if(result[i] > 1+ result[i-x*x])
                result[i] = 1+ result[i-x*x];
            }
        }
        for(int i : result)
        System.out.println(i);
    }

}
