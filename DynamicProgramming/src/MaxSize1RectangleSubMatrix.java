import java.util.Stack;

/**
 * Created by Anmol on 2/8/2016.
 */
//The Problem is different from Max Size square
public class MaxSize1RectangleSubMatrix {

    public static void main(String[] args) {
        int matrix[][] =  new int[][]{{0, 1, 1, 0, 1},
                {1, 1, 0, 1, 0},
                {0, 1, 1, 1, 0},
                {1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1},
                {0, 0, 0, 0, 0}};

        int[] output = new int[matrix[0].length];
        int maxArea = 0;
        for(int i = 0;i<matrix.length;i++) {
            for(int j =0;j<matrix[0].length;j++) {
                if(matrix[i][j] == 0) {
                    output[j] = 0;
                } else {
                    output[j] += matrix[i][j];
                }
            }
           int area = maxAreaHistogram(output, maxArea);
            if(area>maxArea) maxArea = area;
        }
    }

    private static int maxAreaHistogram(int[] output, int maxArea) {
        Stack<Integer> st = new Stack<Integer>();
        int i = 0;
        int pop;
        int area = 0;
        while(i<output.length) {
            if(st.empty()|| output[st.peek()] < output[i]) {
                st.push(i++);
            } else {
                area = getMaxArea(output, st, i, maxArea);
                if(maxArea<area) maxArea = area;
            }
        }
        while (!st.empty()) {
            area = getMaxArea(output, st, i, maxArea);
        }

        System.out.print(area +" ");
        return area;
    }
    private static int getMaxArea(int[] arr, Stack<Integer> st, int i, int maxArea) {
        int pop;
        int area;
        pop = st.pop();
        if(st.empty()) {
            area = arr[pop] * i;
        } else {
            area = arr[pop]* (i - st.peek() -1);
        }
        if(area>maxArea) {
            maxArea = area;
        }
        return maxArea;
    }
}
