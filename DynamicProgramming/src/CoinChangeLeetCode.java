import java.util.Arrays;

/**
 * Created by Anmol on 1/9/2016.
 */
public class CoinChangeLeetCode {

    public static void main(String args[]) {

        int[] coins = new int[]{1,2,5};
        int amount = 11;
        Arrays.sort(coins);

        int result[]= new int[amount+1];
        for(int i = 1;i <=amount;i++) result[i] = -1;
        for (int i=0; i<coins.length; i++)
        {
            for (int S=1; S<=amount; S++) {
                if (S - coins[i]>=0)
                {
                    int minCoinsWithNewCoin;
                    //coin 2 and amount 3
                    if(S-coins[i] > 0 && result[S-coins[i]] <= 0) {
                        continue;
                    } else {
                        minCoinsWithNewCoin = result[S - coins[i]] + 1;
                    }
                    if (minCoinsWithNewCoin < result[S] || result[S]<=0)
                        result[S] = minCoinsWithNewCoin;
                }
            }
        }
        System.out.println("Result: ");
        for(int i : result) {
            System.out.print(i +" ");
        }
        System.out.println(result[amount]);


    }


}
