/**
 * Created by Anmol on 2/9/2016.
 */
public class PowerOfN {

    public static void main(String[] args) {
        int n =7;
        int power = 3;

        int ans = powerOfN(n,power);
        System.out.print(ans);
    }

    private static int powerOfN(int n, int i) {
        if(i == 0) return 1;
        if(i%2 == 0) return powerOfN(n, i/2) * powerOfN(n, i/2);
        else {
            return n* powerOfN(n,i/2) * powerOfN(n,i/2);
        }
    }
}
