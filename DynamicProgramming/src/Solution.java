import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * Created by Anmol on 1/28/2016.
 */
public class Solution {
    public static void main(String[] agrs) {
        int[] arr = new int[]{101, 201, 301, 400, 500, 800, 45, 78, 123, 890, 600, 700};
        int x = 900;
        int d = 100;
        int[] position = new int[x+1];
        boolean[] leaf = new boolean[x+1];
        for(int i = 0; i<arr.length;i++) {
            if(!leaf[arr[i]]){
                position[arr[i]] = i;
                leaf[arr[i]] = true;
            }
        }


        for(int i : position) {
            System.out.print(i + " ");
        }
    }
}


