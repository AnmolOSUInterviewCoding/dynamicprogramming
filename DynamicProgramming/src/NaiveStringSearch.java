/**
 * Created by Anmol on 1/12/2016.
 */
public class NaiveStringSearch {

    public static void main(String[] args) {
        String text = "aaaaaaaaaaaa";
        String pattern = "aa";

        char[] textChar = text.toCharArray();
        char[] patternChar = pattern.toCharArray();
        int count = 0;
        int j;
        for(int i = 0; i <= textChar.length-patternChar.length;i++) {
            if(textChar[i] == patternChar[0]) {

                j= 1;
                int index = i;
                index++;
                if(index == textChar.length) break;
                while(j < patternChar.length) {
                    if(textChar[index] == patternChar[j]) {
                        index++;
                        j++;
                    } else {
                        break;
                    }
                }
                if(j == patternChar.length) {
                    count++;
                    System.out.println("Pattern Found at: " + i);
                }
            }
        }
        System.out.println(count);
    }

}
