/**
 * Created by Anmol on 10/2/2016.
 */
public class CountBits {

    public static void main(String[] args) {
        CountBits countBits = new CountBits();
        int[] result = countBits.countBits(8);
        for(int i : result) {
            System.out.print(i +" ");
        }
    }
    public int[] countBits(int num) {
        if(num == 0) return new int[]{0};
        int[] result = new int[num+1];
        result[0] = 0;
        result[1] = 1;
        int numOfOne = 1;
        int numOfZero = 0;
        for(int i = 2; i<=num;i++) {
            if(numOfZero == 0) {
                numOfZero = numOfOne;
                numOfOne = 1;
            } else {
                numOfOne++;
                numOfZero--;
            }
            result[i] = numOfOne;

        }
        return result;
    }
}
