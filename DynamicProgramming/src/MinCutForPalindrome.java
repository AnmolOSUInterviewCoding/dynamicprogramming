/**
 * Created by Anmol on 1/19/2016.
 */
public class MinCutForPalindrome {

    public static void main(String[] args) {
//        String string ="agbcbm";
        String string ="ababbbabbababa";
        int len = string.length();
        int[][] result = new int[len][len];

        char[] str = string.toCharArray();
        for(int i = 0; i<len;i++){
            result[i][i] = 0;
        }

        //take O(n3) time. There is O(n2) time solution for this problem.
        //“a|babbbab|b|ababa”
        for(int l =2;l<=len;l++) {
            for(int i = 0;i<=len-l;i++) {
                int j = i+l-1;
                int index= i;
                result[i][j] = Integer.MAX_VALUE;
                while(index<=j) {
                    System.out.print(str[index] + " ");
                    index++;
                }
                if(str[i] == str[j]) {
                    result[i][j] = result[i+1][j-1];
                }
                if(result[i][j] != 0) {
                    result[i][j] = Integer.MAX_VALUE;
                    for(int k = i;k<=j-1;k++) {
                        result[i][j] = Math.min(result[i][j],1+ result[i][k]+result[k+1][j]);
                    }

                }
                System.out.println("PArtiotins: " + result[i][j]);
            }

        }
//        System.out.println(result[0][len-1]);
        boolean[][] palindrome = new boolean[len][len];
        for(int l = 2; l<=len;l++) {
            for(int i = 0;i<=len-l;i++) {
                int j = i+l-1;

                if(l==2) {
                    palindrome[i][j] = (str[i] == str[j]);
                } else {
                    palindrome[i][j] = (str[i] == str[j]) &&(palindrome[i+1][j-1]);
                }
            }
        }

        int[] finalTable = new int[len];
        for(int i = 0; i<len;i++) {
            //if palindrome then no cut is required.
            if(palindrome[0][i]) {
                finalTable[i] = 0;
            } else {
                finalTable[i] = Integer.MAX_VALUE;

                for(int j =0; j<i;j++) {
                    if(palindrome[j+1][i] && (1+finalTable[j] <finalTable[i])) {
                        finalTable[i] = 1+ finalTable[j];
                    }

                }
            }
        }
        System.out.println(finalTable[len-1]);
    }
}
