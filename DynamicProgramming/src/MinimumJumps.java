/**
 * Created by Anmol on 1/11/2016.
 */
public class MinimumJumps {

    public static void main(String[] args) {

//        int[] arr = new int[]{1, 3, 5, 8, 9, 2, 6, 7, 6, 8, 9};
        int[] arr = new int[]{1, 3, 6, 1, 0, 9};
        int[] result = new int[arr.length];

        result[0] = 0;

        for(int i =1;i<arr.length;i++) {
            for (int j = 0;j<i;j++) {
                if(i<=j+arr[j]) {
                    result[i] = Math.min(1+ result[j], Integer.MAX_VALUE);
                    break;
                }
            }
        }
        for(int i : result){
            System.out.println(i);
        }

    }

}
