/**
 * @author Anmol on 1/27/2016.
 */
public class SubetSumSubArray {

    public static void main(String[] args) {
        int[] arr = new int[]{3,34,4,12,5,2};
        int sum = 11;
        boolean result[][] = new boolean[sum+1][arr.length+1];
        for(int i = 0; i<=arr.length;i++) {
            result[0][i] = true;
        }
        for(int i = 1;i<=sum;i++) {
            for(int j =1;j<=arr.length;j++) {
                result[i][j] = result[i][j-1];
                if(i>=arr[j-1]) {
                    result[i][j] = result[i][j]||result[i - arr[j-1]][j-1];
                }
            }
        }
        System.out.print("Subset sum present: " + result[sum][arr.length]);
    }

}
