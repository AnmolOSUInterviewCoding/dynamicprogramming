/**
 * Created by Anmol on 2/4/2016.
 */
public class KnapSack {

    public static void main(String[] args) {
        int val[] = new int[]{60, 100, 120};
        int wt[] = new int[]{10, 20, 30};
        int  W = 50;

        int[][] result = new int[W+1][val.length+1];
        for(int i =1;i<=W;i++) {
            for(int j = 1;j<=val.length;j++) {
                if(i >=wt[j-1]) {
                    result[i][j] = Math.max(val[j-1] + result[i - wt[j-1]][j-1], result[i][j-1]);
                } else {
                    result[i][j]= result[i][j-1];
                }
            }
        }
    }
}
