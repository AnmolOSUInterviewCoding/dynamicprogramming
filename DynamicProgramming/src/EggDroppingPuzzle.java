/**
 * Created by Anmol on 1/19/2016.
 */
public class EggDroppingPuzzle {

    public static void main(String[] args) {

        int numberOfEggs = 2;
        int numberOfFloors = 100;

        int[][] result = new int[numberOfEggs+1][numberOfFloors+1];

        for(int i = 1; i<=numberOfEggs;i++) {
            for(int j = 1;j<=numberOfFloors;j++) {

                if(i ==1) {
                    result[i][j] = j;
                } else {
                    int min = Integer.MAX_VALUE;
                    for(int k =1; k<=j;k++) {
                        min = Math.min(1 + Math.max(result[i-1][k-1], result[i][j-k]), min);
                    }
                    result[i][j] = min;
                }

            }
        }
        System.out.print(result[numberOfEggs][numberOfFloors]);

    }

}

