import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Anmol on 2/22/2016.
 */
public class ArraySum {

    public static void main(String[] args) {
        int arr1[] = new int[]{9,9,2};
        int arr2[] = new int[]{1,0,1,3};

        sumArray(arr1, arr2);
    }

    private static void sumArray(int[] arr1, int[] arr2) {

        int i = arr1.length-1;
        int j = arr2.length-1;
        List<Integer> list = new ArrayList<Integer>();
        int carry = 0;
        while (i>=0&&j>=0) {
            int sum = carry + arr1[i]+ arr2[j];
            if(sum>=10) {
                carry = 1;
            } else {
                carry =0;
            }
            list.add(sum%10);
            i--;
            j--;
        }

        if(j>=0&&i<0) {
            while (j>=0) {
                int sum = carry + arr2[j];
                if(sum>=10) {
                    carry = 1;
                } else {
                    carry =0;
                }
                list.add(sum%10);
                j--;
            }
        } else if(i>=0&&j<0) {
            while (i>=0) {
                int sum = carry + arr1[i];
                if(sum>=10) {
                    carry = 1;
                }else {
                    carry =0;
                }
                list.add(sum%10);
                i--;
            }
        }
        if(carry ==1) list.add(carry);

        Collections.reverse(list);

    }
}
