import java.util.ArrayList;

/**
 * Created by Anmol on 2/7/2016.
 */
public class UglyNumber2LeetCode {

    public static void main(String[] args) {
        int n = 7;
        /*if(n <= 0) return 0;
        if(n == 1) return 1;
        if(n == 2) return 2;
        if(n == 3) return 3;*/

        /*ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(5);

        for(int i= 4;list.size()<=n && i<2147483647;i++) {
            if(i%2 == 0){
                if(list.contains(i/2)) {
                    list.add(i);
                    continue;
                }
            }
            if(i%3 == 0){
                if(list.contains(i/3)) {
                    list.add(i);
                    continue;
                }
            }
            if(i%5 == 0){
                if(list.contains(i/5)) {
                    list.add(i);
                }
            }
        }*/
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        int n2 = 0;
        int n3 = 0;
        int n5 = 0;
        int multipleOf2 = 2;
        int multipleOf5 = 5;
        int multipleOf3 = 3;
        for(int i =2; i<=n&& i<2147483647;i++) {
            int nextNumber = Math.min(Math.min(multipleOf2, multipleOf3), multipleOf5);
            list.add(nextNumber);

            if(nextNumber == multipleOf2) {
                multipleOf2 = list.get(++n2)*2;
            }
            if(nextNumber == multipleOf3) {
                multipleOf3 = list.get(++n3)*3;
            }if(nextNumber == multipleOf5) {
                multipleOf5 = list.get(++n5)*5;
            }

        }
//        return list.get(n);
    }
}
