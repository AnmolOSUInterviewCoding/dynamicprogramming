import java.util.HashMap;
import java.util.Map;

/**
 * @author by Anmol on 2/8/2016.
 */
public class HackerRankRajReverseIntegers {
    public static void main(String[] agrs) {
        int number1 = 4358;
        int number2 = 750;

        int reverseSum = calculateReverseSum(number1, number2);
        System.out.print(reverseSum);
    }

    private static int calculateReverseSum(int number1, int number2) {
        int reverseNumber1 = reverseNumber(number1);
        int reverseNumber2 = reverseNumber(number2);
        return reverseNumber(reverseNumber1 + reverseNumber2);
    }

    private static int reverseNumber(int number1) {
        int temp = 0;
        while(number1!=0) {
            temp = temp*10 + number1%10;
            number1 = number1/10;
        }
        return temp;
    }
}