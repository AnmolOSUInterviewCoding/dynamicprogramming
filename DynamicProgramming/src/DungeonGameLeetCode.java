/**
 * Created by Anmol on 2/9/2016.
 */
public class DungeonGameLeetCode {

    public static void main(String[] args) {
        int[][] dungeon = new int[][] {{-2,-3,3},
                                    {-5,-10,1},
                                    {10,30,-5}};

        int m = dungeon.length;
        int n = dungeon[0].length;

        //init dp table
        int[][] h = new int[m][n];

        h[m - 1][n - 1] = Math.max(1 - dungeon[m - 1][n - 1], 1);

        //init last column
        for (int i = m - 2; i >= 0; i--) {
            h[i][n - 1] = Math.max(h[i + 1][n - 1] - dungeon[i][n - 1], 1);
        }

        //init last row
        for (int j = n - 2; j >= 0; j--) {
            h[m - 1][j] = Math.max(h[m - 1][j + 1] - dungeon[m - 1][j], 1);
        }

        //calculate dp table
        for (int i = m - 2; i >= 0; i--) {
            for (int j = n - 2; j >= 0; j--) {
                int down = Math.max(h[i + 1][j] - dungeon[i][j], 1);
                int right = Math.max(h[i][j + 1] - dungeon[i][j], 1);
                h[i][j] = Math.min(right, down);
            }
        }
        for(int i = 0;i<m;i++) {
            for(int j = 0; j<n;j++) {
                System.out.print(h[i][j] +" ");
            }
            System.out.println();
        }
    }
}
