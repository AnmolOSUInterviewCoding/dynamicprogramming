/**
 * Created by Anmol on 2/6/2016.
 */
public class LongestCommonSubstring {

    public static void main(String[] agrs) {
        String s1  = "abcdfef";
        String s2 = "gebcdf";
        int[][] result = new int[s1.length()+1][s2.length()+1];
        for(int i = 1; i<=s1.length();i++) {
            for(int j = 1;j<=s2.length();j++) {
                if(s1.charAt(i-1)== s2.charAt(j-1)) {
                    result[i][j] = 1 + result[i-1][j-1];
                } else {
                    result[i][j] = result[i-1][j-1];
                }
            }
        }
        int max =0;
        int row = 0, col = 0;
        for(int i = 0;i<result.length;i++) {
            for(int j = 0; j<result[0].length;j++) {
                if(max< result[i][j]) {
                    max = result[i][j];
                    row = i;
                    col = j;
                }
            }
        }
        System.out.print(row + " " + col +" " + max +"\n");
        int count =0;
        while(count!=3) {
            System.out.print(s1.charAt(row -count-1) +" ");
            count++;
        }
    }

}
