/**
 * Created by Anmol on 1/14/2016.
 */
public class ReverseWordsString {

    public static void main(String[] args) {
//        if(s.length()==0) return "";
        String s = "The Sky Is  Blue";
        String[] split = s.split(" ");
        int j = split.length -1;
        StringBuilder my = new StringBuilder();
        for(int i = 0; i<=j;i++) {
            if(split[i].equals("")){
                continue;
            } else {
                String temp = split[i];
                split[i] = split[j];
                split[j] = temp;
                j--;
            }

        }
        for(int i =0; i< split.length -1;i++) {
            if(!split[i].equals("")) {
                my.append(split[i]);
                my.append(" ");
            }
        }
        my.append(split[split.length -1]);
        System.out.println(my );
    }

}
