/**
 * Created by Anmol on 2/6/2016.
 */
public class SubsetSum {

    public static void main(String[] args) {
        int[] arr = new int[]{2,3,7,8,10};
        int sum = 11;
        boolean[][] result= new boolean[sum+1][arr.length+1];

        for(int i = 0; i<=arr.length;i++) {
            result[0][i] = true;
        }

        for(int i = 1;i<=sum;i++) {
            for(int j= 1; j<=arr.length;j++) {
                if(i >= arr[j-1]) {
                    result[i][j] = result[i][j-1]|| result[i -arr[j-1]][j-1];
                } else {
                    result[i][j] = result[i][j-1];
                }
            }
        }

    }
}
