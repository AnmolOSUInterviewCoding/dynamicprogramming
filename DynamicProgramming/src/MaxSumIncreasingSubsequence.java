/**
 * Created by Anmol on 1/11/2016.
 */
public class MaxSumIncreasingSubsequence {

    public static void main(String[] args) {
//        int[] arr = new int[] {1, 101, 2, 3, 100, 4, 5};
        int[] arr = new int[] {3, 4, 5, 10};

        int result[] = new int[arr.length];
        result[0] = arr[0];
        int sum = 0;
        int maxSum = Integer.MIN_VALUE;
        for(int i = 1;i <arr.length;i++) {
            sum = arr[i];
            for(int j = 0;j<i;j++) {
                if(arr[i]>arr[j]) {
                    sum = sum+arr[j];
                }
            }
            if(sum > maxSum) {
                maxSum = sum;
            }
        }
        System.out.println(maxSum);
    }

}
